package com.example.merkeyados;

import android.app.Application;
import io.realm.Realm;
import io.realm.RealmConfiguration;

class MyApplicationR extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Realm.init(this);
        RealmConfiguration config = new RealmConfiguration.Builder()
                .name("jovenescreativos.realm")
                .deleteRealmIfMigrationNeeded()
                .build();
        Realm.setDefaultConfiguration(config);
    }
}
